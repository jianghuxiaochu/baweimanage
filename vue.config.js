const path = require('path');

function addStyleResource(rule) {
	rule
		.use('style-resource')
		.loader('style-resources-loader')
		.options({
			patterns: [path.resolve(__dirname, './src/assests/common/common.scss')],
		});
}
module.exports = {
	lintOnSave: process.env.NODE_ENV === 'development',
	devServer: {
		proxy: {
			'/dev-api': {
				target: process.env.VUE_APP_BASE_URL + '/dev-api',
			},
		},
	},
	chainWebpack: config => {
		const types = ['vue-modules', 'vue', 'normal-modules', 'normal'];
		types.forEach(type =>
			addStyleResource(config.module.rule('scss').oneOf(type)),
		);
	},
};
