import axios from 'axios';
import { ElMessage } from 'element-plus';

import code from './httpCode';

const Axios = axios.create({
	baseURL: process.env.VUE_APP_BASE_URL,
	timeout: 300000,
});

// 添加请求拦截器
Axios.interceptors.request.use(
	function (config) {
		// 在发送请求之前做些什么
		let whitelist = ['/dev-api/captchaImage', '/dev-api/login'];
		if (whitelist.some(item => item.includes(config.url))) {
			return config;
		} else {
			let token = window.localStorage.getItem('token') || '';
			config.headers['authorization'] = token;
			return config;
		}
	},
	function (error) {
		// 对请求错误做些什么
		return Promise.reject(error);
	},
);

// 添加响应拦截器
Axios.interceptors.response.use(
	function (response) {
		// 对响应数据做点什么

		const { data } = response;
		if (data.code === 200) {
			return Promise.resolve(response);
		} else {
			ElMessage.error(data.msg);
			return Promise.reject(response);
		}
	},
	function (error) {
		// 如果页面加载超时未获取到数据，给用户以提示刷新页面重新加载
		console.log('您的请求已超时请重新获取数据');
		switch (error.code) {
			case 'ECONNABORTED':
				{
					ElMessage.error('您的请求已超时请重新获取数据');
				}
				break;
		}
		// 如果返回的是其他 code 码，则按照下面的提示
		code[error.code] && ElMessage.error(code[error.code]);

		// 对响应错误做点什么
		return Promise.reject(error);
	},
);
export default Axios;
