import api from '@/api';
const { waitWork } = api;

export default {
	// namespaced:true,    命名空间  防止命名冲突
	state: () => ({
		backLogData: {}, //  我的待办的数据
		blankData: {}, //  待办问答总数据
		bankTotal: '',
	}),
	actions: {
		// 获取班级数据
		getClassInfo: async ({ commit }) => {
			const { data } = await waitWork._getClassInfo();
			if (data.code === 200) {
				// console.log(data);
				commit('getWorkList', data);
			}
		},
		// 获取待办问答数据
		getBlankData: async ({ commit }, payload = {}) => {
			console.log(payload);

			const { data } = await waitWork._getBlackList(payload);
			if (data.code === 200) {
				console.log(data);
				commit('getBlankList', data);
			}
		},
	},
	mutations: {
		getWorkList(state, data) {
			state.backLogData = data;
		},
		getBlankList(state, data) {
			state.blankData = data;
			state.bankTotal = data.total;
		},
	},
};
