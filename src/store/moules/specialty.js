import api from '@/api';
const { homeData } = api;

export default {
	// namespaced:true,    命名空间  防止命名冲突
	state: () => ({
		publicSpecialtyList: [], //  专业的数据
		blackListWaitList: {}, //  待办的数据
	}),
	actions: {
		// 获取专业数据
		getSpecialty: async ({ commit }) => {
			const { data } = await homeData.selectStationLabel();
			if (data.code === 200) {
				// console.log(data.data);
				commit('createSpecialty', data.data);
			}
		},
		// 获取待办数据
		getBlackListWaitList: async ({ commit }) => {
			const { data } = await homeData.blackListWaitting();
			if (data.code === 200) {
				// console.log(data);
				commit('getBlackListWaitList', data);
			}
		},
	},
	mutations: {
		createSpecialty(state, data) {
			state.publicSpecialtyList = data;
		},
		getBlackListWaitList(state, data) {
			state.blackListWaitList = data;
		},
	},
};
