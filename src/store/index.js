import { createStore } from 'vuex';

import specialtyData from './moules/specialty';
import backlogDataLists from './moules/backlog';

// 创建一个新的 store 实例
const store = createStore({
	state() {
		return {};
	},
	mutations: {},
	actions: {},
	modules: {
		specialtyData,
		backlogDataLists,
	},
});

export default store;
