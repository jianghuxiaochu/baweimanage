import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import ElementUi from '@/plugin/elementUi.js';

import VMdEditor from '@kangc/v-md-editor';
import '@kangc/v-md-editor/lib/style/base-editor.css';
import vuepressTheme from '@kangc/v-md-editor/lib/theme/vuepress.js';
import '@kangc/v-md-editor/lib/theme/style/vuepress.css';
import 'element-plus/lib/theme-chalk/index.css';
// Prism
import Prism from 'prismjs';
// highlight code
import 'prismjs/components/prism-json';
import store from '@/store';
VMdEditor.use(vuepressTheme, {
	Prism,
});

createApp(App)
	.use(VMdEditor)
	.use(router)
	.use(ElementUi)
	.use(store)
	.mount('#app');
