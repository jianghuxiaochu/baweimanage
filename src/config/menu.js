import moment from 'moment';
// 学生的导航菜单
const menuStudent = [
	{
		id: 1,
		path: '/home/post',
		text: '岗位',
		status: 1,
	},
	{
		id: 2,
		path: '/home/project',
		text: '项目',
		status: 1,
	},
	{
		id: 3,
		path: '/home/training',
		text: '实训',
	},
	{
		id: 4,
		path: '/home/interview',
		text: '面试',
		status: 1,
		children: [
			{
				id: 1,
				path: '/home/interview/record',
				text: '面试记录',
			},
			{
				id: 2,
				path: '/home/interview/myRecord',
				text: '我的面试记录',
			},
			{
				id: 3,
				path: '/home/interview/ranking',
				text: '面试排行榜',
			},
		],
	},
	{
		id: 5,
		path: '/home/answer',
		text: '问答',

		children: [
			{
				id: 1,
				path: '/home/answer/answerList',
				text: '问答列表',
			},
			{
				id: 2,
				path: '/home/answer/myanswer',
				text: '我的问答',
			},
		],
	},
];

// 教师的导航菜单
const menuTeacher = [
	{
		id: 1,
		path: '/home/post',
		text: '岗位',
		status: 1,
	},
	{
		id: 2,
		path: '/home/project',
		text: '项目',
		status: 1,
	},
	{
		id: 3,
		path: '/home/training',
		text: '实训',
		child: [
			{
				id: 1,
				path: '/home/training/planList',
				text: '计划',
			},
			{
				id: 2,
				path: '/home/training/viewPlan',
				text: '进度',
			},
			{
				id: 3,
				path: '/home/training/defence',
				text: '答辩',
			},
			{
				id: 4,
				path: '/home/training/planListManage',
				text: '计划(管理)',
			},
		],
	},
	{
		id: 4,
		path: '/home/interview',
		text: '面试',
		status: 1,
		children: [
			{
				id: 1,
				path: '/home/interview/record',
				text: '面试记录',
			},
			{
				id: 2,
				path: '/home/interview/interviewmanage',
				text: '面试记录管理',
			},
			{
				id: 3,
				path: '/home/interview/ranking',
				text: '面试排行榜',
			},
		],
	},
	{
		id: 5,
		path: '/home/answer',
		text: '问答',

		children: [
			{
				id: 1,
				path: '/home/answer/answerList',
				text: '问答列表',
			},
			{
				id: 2,
				path: '/home/answer/myanswer',
				text: '问答管理',
			},
		],
	},
];
// 岗位 学生端表格表头
const postStudentColumns = [
	{
		prop: 'name',
		label: '岗位',
	},
	{
		prop: 'majorName',
		label: '学院',
	},
	{
		prop: 'stationVersion',
		label: '版本号',
	},
	{
		prop: 'skillNum',
		label: '技能数量',
	},
	{
		prop: 'userName',
		label: '作者',
	},
	{
		prop: 'createTime',
		label: '发起时间',
		formatter(cellValue) {
			// console.log(row, column, cellValue, index);
			return moment(cellValue).format('YY-MM-DD hh:mm:ss');
		},
	},
	{
		label: '操作',
		slot: {
			title: 'optionheader',
			body: 'optionrow',
		},
	},
];
// 岗位 教师端表格表头
const postTeacherColumns = [
	{
		prop: 'name',
		label: '岗位',
	},
	{
		prop: 'majorName',
		label: '学院',
	},
	{
		prop: 'stationVersion',
		label: '版本号',
	},
	{
		prop: 'skillNum',
		label: '技能数量',
	},
	{
		prop: 'userName',
		label: '作者',
	},
	{
		prop: 'createTime',
		label: '发起时间',
		formatter(cellValue) {
			// console.log(row, column, cellValue, index);
			return moment(cellValue).format('YY-MM-DD hh:mm:ss');
		},
	},
	{
		prop: 'status',
		label: '状态',
		formatter(row, column, cellValue, index) {
			console.log(row, column, cellValue, index);
			switch (cellValue) {
				case '1':
					return <div style='color: #999;'>草稿</div>;

				case '2':
					return <div style='color: #29CB97;'>已发布</div>;

				case '3':
					return <div style='color: #679CF6;'>待审核</div>;

				case '4':
					return <div style='color: red;'>已驳回</div>;

				default:
					break;
			}
		},
	},
	{
		label: '操作',
		slot: {
			title: 'optionheader',
			body: 'optionrow',
		},
	},
];

// 岗位状态数据
const postTypeList = [
	{
		id: 1,
		title: '全部',
		isAsc: 'desc',
		pageNum: 1,
		pageSize: 10,
		searchTitle: null,
		majorId: null,
		status: null,
		isMyInfo: false,
	},
	{
		id: 2,
		title: '草稿',
		isAsc: 'desc',
		pageNum: 1,
		pageSize: 10,
		searchTitle: null,
		majorId: 'P0001',
		status: 1,
		isMyInfo: false,
	},
	{
		id: 3,
		title: '已发布',
		isAsc: 'desc',
		pageNum: 1,
		pageSize: 10,
		searchTitle: null,
		majorId: 'P0002',
		status: 2,
		isMyInfo: false,
	},
	{
		id: 4,
		title: '待审核',
		isAsc: 'desc',
		pageNum: 1,
		pageSize: 10,
		searchTitle: null,
		majorId: 'P0003',
		status: 3,
		isMyInfo: false,
	},
	{
		id: 5,
		title: '已驳回',
		isAsc: 'desc',
		pageNum: 1,
		pageSize: 10,
		searchTitle: null,
		majorId: 'P0004',
		status: 4,
		isMyInfo: false,
	},
];

// 教师端表头数据项目
const projectTeacherColumns = [
	{
		prop: 'proname',
		label: '项目名称',
	},
	{
		prop: 'versionNum',
		label: '版本',
	},
	{
		prop: 'taskCount',
		label: '任务数量',
	},
	{
		prop: 'major',
		label: '所属专业',
	},
	{
		prop: 'industry',
		label: '所属行业',
	},
	{
		prop: 'sxtype',
		label: '实训类型',
		formatter(row, column, cellValue, index) {
			console.log(row, column, cellValue, index);
			switch (cellValue) {
				case null:
					return <div style='color: #000;'>专业群实训</div>;
				default:
					break;
			}
		},
	},
	{
		prop: 'createTim',
		label: '推荐完成天数',
	},
	{
		prop: 'updateTime',
		label: '更新时间',
	},
	{
		prop: 'demonstration',
		label: '演示',
		formatter(cellValue) {
			// console.log(row, column, cellValue, index);
			switch (cellValue) {
				case undefined:
					return <div style='color: #999;'>查看</div>;
				default:
					break;
			}
		},
	},
	{
		prop: 'status',
		label: '状态',
		formatter(row, column, cellValue, index) {
			console.log(row, column, cellValue, index);
			switch (cellValue) {
				case '1':
					return <div style='color: #999;'>草稿</div>;

				case '2':
					return <div style='color: #29CB97;'>已发布</div>;

				case '3':
					return <div style='color: #679CF6;'>待审核</div>;

				case '4':
					return <div style='color: red;'>已驳回</div>;

				default:
					break;
			}
		},
	},
	{
		label: '操作',
		slot: {
			title: 'optionheader',
			body: 'optionrow',
		},
	},
];

// 岗位--------> 我的待办问答数据表格表头
const bankColumns = [
	{
		prop: 'questionTitle',
		label: '待办事项',
	},
	{
		prop: 'type',
		label: '类型',
	},
	{
		prop: 'qUserName',
		label: '发起人',
	},
	{
		prop: 'createTime',
		label: '发起时间',
		formatter(cellValue) {
			// console.log(row, column, cellValue, index);
			return moment(cellValue).format('YY-MM-DD hh:mm:ss');
		},
	},
	{
		label: '操作',
		slot: {
			title: 'optionheader',
			body: 'optionrow',
		},
	},
];

export default {
	menuStudent,
	menuTeacher,
	postStudentColumns,
	postTeacherColumns,
	postTypeList,
	projectTeacherColumns,
	bankColumns,
};
