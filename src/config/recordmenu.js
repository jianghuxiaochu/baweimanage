const rankingmenu = [
	{
		prop: 'rangking',
		label: '排名',
	},
	{
		prop: 'commitName',
		label: '提交人',
	},
	{
		prop: 'count',
		label: '面试记录提交数',
	},
	{
		prop: 'className',
		label: '班级',
	},
	{
		prop: 'majorName',
		label: '专业',
	},
];
const recordmenulist = [
	{
		prop: 'stationName',
		label: '岗位名称',
	},
	{
		prop: 'companyName',
		label: '公司名称',
	},
	{
		prop: 'interviewTime',
		label: '面试时间',
	},
	{
		prop: 'majorName',
		label: '专业',
	},
	{
		prop: 'commitName',
		label: '提交人',
	},
	{
		prop: 'status',
		label: '面试结果',
		formatter(row, column, cellValue) {
			switch (cellValue) {
				case 0:
					return <span>不确定</span>;
				case 1:
					return <span style='color: #29cb97;font-weight: 400;'> 通过 </span>;
				case 2:
					return <span style='color: #e34949;font-weight: 400;'> 未通过 </span>;
			}
		},
	},
	{
		prop: 'issoundrecord',
		label: '录音文件',
		formatter(row, column, cellValue) {
			switch (cellValue) {
				case 0:
					return <span>无</span>;
				case 1:
					return <span> 有 </span>;
			}
		},
	},
];
export default {
	rankingmenu,
	recordmenulist,
};
