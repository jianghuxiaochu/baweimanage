//------------------------------学生端----------------------------------
// 我的问题列表数据 get
export const mysrk = {
	url: '/dev-api/sypt/answer/myAsk?',
	method: 'get',
};
// 我的答案列表数据
export const myanswer = {
	url: '/dev-api/sypt/answer/myAnswer?',
	method: 'get',
};
// 删除接口
export const answerdelete = {
	url: '/dev-api/sypt/answer/delete?',
	method: 'delete',
};
// 点击编辑之后回显数据的接口
export const selectContextByAnswerId = {
	url: '/dev-api/sypt/answer/selectContextByAnswerId?',
	method: 'get',
};
// 点击弹框的确定按钮之后调用的接口
export const update = {
	url: '/dev-api/sypt/answer/update',
	method: 'post',
};

//---------------教师端----------------

export const answerwait = {
	url: '/dev-api/sxpt/answer/wait?',
	method: 'get',
};
export const answerall = {
	url: '/dev-api/sxpt/answer/all?',
	method: 'get',
};
// 批量回答
export const replyRightAnswer = {
	url: '/dev-api/sxpt/answer/replyRightAnswer',
	method: 'post',
};
// 正确答案的类似问题
export const selectAnswerList = {
	url: '/dev-api/sxpt/askAndAnswer/selectAnswerList?',
	method: 'get',
};
// 设为精品
export const quality = {
	url: '/dev-api/sxpt/answer/quality?',
	method: 'get',
};
// 取消精品
export const deleteQuality = {
	url: '/dev-api/sxpt/answer/deleteQuality?',
	method: 'get',
};
// 屏蔽成功
export const shield = {
	url: '/dev-api/sxpt/answer/shield?',
	method: 'get',
};
// 取消屏蔽成功
export const deleteShield = {
	url: '/dev-api/sxpt/answer/deleteShield?',
	method: 'get',
};
// 认证答案
export const authenticationReply = {
	url: '/dev-api/sypt/reply/authenticationReply?',
	method: 'get',
};
