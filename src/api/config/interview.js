// 面试
export const getinterviewData = {
	url: '/dev-api/sxpt/station/selectStationLabel',
	method: 'get',
};

// 分页
export const getinterviewpage = {
	url: '/dev-api/sypt/interview/interviewList',
	method: 'get',
};

// 我的面试记录
export const getmyrecord = {
	url: '/dev-api/sypt/interview/myInterviewList',
	method: 'get',
};

// 面试排行榜
export const getranklist = {
	url: '/dev-api/sypt/interview/interviewRecordRangking',
	method: 'get',
};
// 面试题榜单
export const answerRangkinglist = {
	url: '/dev-api/sypt/interview/interviewAnswerRangking',
	method: 'get',
};

// 教师端

// 面试记录管理
export const getinterviewManagelist = {
	url: '/dev-api/sypt/interview/interviewManage',
	method: 'get',
};

// 屏蔽状态
export const getstatus = {
	url: '/dev-api/sypt/interview/deleteInterview',
	method: 'get',
};

//  排行榜  点击班级
export const getclass = {
	url: '/dev-api/sxpt/interview/interviewRecordRangkingTeacher',
	method: 'get',
};

//点击 面试题榜单
export const getrecordbang = {
	url: '/dev-api/sxpt/interview/interviewAnswerRangkingTeacher',
	method: 'get',
};
