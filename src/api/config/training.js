// 实训接口

// 收藏项目
export const collectProject = {
	url: '/dev-api/sypt/project/favor/list',
	method: 'get',
};

// 相关实训
export const relatedTraining = {
	url: '/dev-api/sypt/project/list',
	method: 'get',
};

// 切换状态
export const selectClassPlanByStu = {
	url: '/dev-api/sxpt/progress/selectClassPlanByStu',
	method: 'get',
};

// 展开数据
export const selectClassPlanByStuInfo = {
	url: `/dev-api/sxpt/progress/selectClassPlanByStuInfo/:id`,
	method: 'get',
};
