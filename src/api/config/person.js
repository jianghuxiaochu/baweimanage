export const getStudentPersonInfo = {
	url: '/dev-api/system/user/profile',
	method: 'get',
};

export const editUserProfile = {
	url: '/dev-api/system/user/profile',
	method: 'put',
};

export const getPersonInfo = id => {
	return {
		url: `/dev-api/sxpt/station/selectStationListById/${id}`,
		method: 'get',
	};
};
