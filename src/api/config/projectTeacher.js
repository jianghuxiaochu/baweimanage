// 教师端
// 分页数据
export const selectProjectList = {
	url: 'dev-api/sxpt/project/selectProjectList',
	method: 'get',
};

// 升级
export const promoteProject = {
	url: `/dev-api/sxpt/project/promoteProject`,
	method: 'get',
};

export const teacherdetail = {
	url: '/dev-api/sxpt/project/selectProjectByVserionId',
	method: 'get',
};
// 详情接口
