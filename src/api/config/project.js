// 行业
export const getTradeList = {
	url: '/dev-api/sxpt/label/selectTradeList',
	method: 'get',
};

// 专业
export const getMajorStationList = {
	url: '/dev-api/sxpt/label/selectMajorStationList',
	method: 'get',
};

// 分页数据
export const getPaginList = {
	url: '/dev-api/sypt/project/list',
	method: 'get',
};

// 项目详情页面
//获取tab切换的值
export const tabs = {
	url: '/dev-api/sxpt/brief/selectBriefTree',
	method: 'get',
};

//获取评论数据
export const pinglun = {
	url: '/dev-api/sypt/discuss/list',
	method: 'get',
};

//获取发表评论
export const fabu = {
	url: '/dev-api/sypt/discuss',
	method: 'post',
};

// 指导老师
export const teacher = {
	url: '/dev-api/sypt/project/teacherInfo/:id',
	method: 'get',
};
