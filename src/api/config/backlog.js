export const _getClassInfo = {
	url: '/dev-api/sxpt/classPlan/getClassInfo',
	method: 'get',
};

export const _getBlackList = {
	url: '/dev-api/sxpt/blacking/blackList',
	method: 'get',
};
