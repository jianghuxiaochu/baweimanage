// 专业数据接口
export const selectStationLabel = {
	url: '/dev-api/sxpt/station/selectStationLabel',
	method: 'get',
};

export const blackListWaitting = {
	url: '/dev-api/sxpt/blacking/blackList',
	method: 'get',
};

export const selectStationNewVersionList = {
	url: '/dev-api/sypt/station/selectStationNewVersionList',
	method: 'get',
};

export const selectStationVersionList = {
	url: '/dev-api/sxpt/station/selectStationVersionList',
	method: 'get',
};

// export const deleteStationItem = {
// 	url: '/dev-api/sxpt/station/deleteStation',
// 	method: 'delete',
// };

// dev-api/sxpt/station/deleteStation
