// 列表数据
export const answer = {
	url: '/dev-api/sypt/answer/list?',
	method: 'get',
};
// 跳详情
export const anDetail = {
	url: '/dev-api/sypt/answer/',
	method: 'get',
};
// 列表详情评论 post
export const reply = {
	url: '/dev-api/sypt/answer/reply',
	method: 'post',
};
// 回答
export const alList = {
	url: '/dev-api/sypt/reply/answerList?',
	method: 'get',
};
// 收藏
export const collectionadd = {
	url: '/dev-api/sypt/collection/add?',
	method: 'get',
};
// 取消收藏
export const collectiondelete = {
	url: '/dev-api/sypt/collection/delete?',
	method: 'delete',
};
// 添加
export const answeradd = {
	url: '/dev-api/sypt/answer',
	method: 'post',
};

// 我的回答数量数据
export const question = {
	url: '/dev-api/sypt/answer/reply',
	method: 'get',
};
