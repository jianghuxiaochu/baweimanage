import Axios from '../util/httptool';
import * as loginConfig from './config/login.js';
import * as homeConfig from './config/post';
import * as projectConfig from './config/project';
import * as projectTeacher from './config/projectTeacher';
import * as answer from './config/answer';
import * as mysrk from './config/myanswer';
import * as interviewConfig from './config/interview';
import * as addRecord from './config/addRecord';
import * as studentPerson from './config/person';
import * as trainingConfig from './config/training';
import * as backlog from './config/backlog';

/*--------------------------------登录和岗位模块---------------------------------*/
// 登录
const loginData = Object.keys(loginConfig).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...loginConfig[key], data });
	return val;
}, {});

// 我的待办
const waitWork = Object.keys(backlog).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...backlog[key], params: data });
	return val;
}, {});

// 岗位
const homeData = Object.keys(homeConfig).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...homeConfig[key], params: data });
	return val;
}, {});
// 删除岗位
// const delStations = (data={}) => Axios({
// 	url:`/dev-api/sxpt/station/deleteStation/${data.stationVersionId}`,
// 	method: 'DELETE',
// 	data
// })
const delStations = id =>
	Axios.delete('/dev-api/sxpt/station/deleteStation', {
		params: { stationVersionId: id },
	});
// 个人中心
const personData = Object.keys(studentPerson).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...studentPerson[key], data });
	return val;
}, {});

const personDetailInfo = id =>
	Axios.delete(
		`/dev-api/sxpt/station/selectStationListById/stationVersionId=${id}`,
	);

// Axios({
// 	url: `/dev-api/sxpt/station/selectStationListById/${data.stationVersionId}`,
// 	method: 'get',
// 	data,
// });
/*--------------------------------面试---------------------------------*/
// 面试
const interviewData = Object.keys(interviewConfig).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...interviewConfig[key], params: data });
	return val;
}, {});

// 面试添加
const addInterviewList = Object.keys(addRecord).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...addRecord[key], data });
	return val;
}, {});

// 面试
const recordlist = Object.keys(interviewConfig).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...interviewConfig[key], params: data });
	return val;
}, {});

// 面试删除
const deletelist = params =>
	Axios.delete('/dev-api/sypt/interview/deleteInterviewById/' + params);

// 面试详情
const details = params => Axios.get('/dev-api/sypt/interview/info/' + params);

/*--------------------------------项目模块---------------------------------*/

//*********************************学生端**********************************
// 项目
const projectlistData = Object.keys(projectConfig).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...projectConfig[key], data });
	return val;
}, {});

//get
const getData = Object.keys(projectConfig).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...projectConfig[key], params: data });
	return val;
}, {});

// *******************************教师端*********************************

const projectTeapost = Object.keys(projectTeacher).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...projectTeacher[key], data });
	return val;
}, {});

const projectTeaget = Object.keys(projectTeacher).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...projectTeacher[key], params: data });
	return val;
}, {});

/*--------------------------------实训模块---------------------------------*/

// 实训
const traininglistData = Object.keys(trainingConfig).reduce((val, key) => {
	// console.log(key);
	val[key] = (data = {}) => Axios({ ...trainingConfig[key], data });
	return val;
}, {});

const gettrainingData = Object.keys(trainingConfig).reduce((val, key) => {
	// console.log(key);
	val[key] = (data = {}) => Axios({ ...trainingConfig[key], params: data });
	return val;
}, {});

/*--------------------------------问答模块---------------------------------*/
// 问答列表数据 get
const answerlist = Object.keys(answer).reduce((val, key) => {
	// console.log(key);
	val[key] = (data = {}) => Axios({ ...answer[key], params: data });
	return val;
}, {});
// 问答列表添加 post
const answeraddd = Object.keys(answer).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...answer[key], data });
	return val;
}, {});

// 我的列表跳详情数据 get
const ansDetail = Object.keys(answer).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...answer[key], params: data });
	return val;
}, {});
// 我的收藏 get
const collectionadd = Object.keys(answer).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...answer[key], params: data });
	return val;
}, {});
// 取消收藏 get
const collectiondelete = Object.keys(answer).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...answer[key], params: data });
	return val;
}, {});

// 列表详情评论 post
const reply = Object.keys(answer).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...answer[key], data });
	return val;
}, {});
// 我的详情回答数据 get
const allList = Object.keys(answer).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...answer[key], params: data });
	return val;
}, {});
// 我的问答数量数据 get
const questionlist = Object.keys(answer).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...answer[key], ...data });
	return val;
}, {});
/*-----------------------------------------------------------------*/
//-----------------------------学生端----------------------------------
// 我的问题列表数据 get
const mysrkList = Object.keys(mysrk).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...mysrk[key], params: data });
	return val;
}, {});
// 我的答案列表数据 get
const myanswer = Object.keys(mysrk).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...mysrk[key], params: data });
	return val;
}, {});
// 我的问题列表数据 delete
const answerdelete = Object.keys(mysrk).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...mysrk[key], params: data });
	return val;
}, {});
// 我的问题列表数据 get
const selectContextByAnswerId = Object.keys(mysrk).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...mysrk[key], params: data });
	return val;
}, {});
// 我的问题列表数据 post
const update = Object.keys(mysrk).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...mysrk[key], data });
	return val;
}, {});
//----------------------教师端------------------------
// 待处理问答列表数据 get
const answerwait = Object.keys(mysrk).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...mysrk[key], params: data });
	return val;
}, {});
// 所有问答列表数据 get
const aswerall = Object.keys(mysrk).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...mysrk[key], params: data });
	return val;
}, {});
// 所有问答列表数据 post
const replyRightAnswer = Object.keys(mysrk).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...mysrk[key], data });
	return val;
}, {});
// 所有问答列表数据 get
const selectAnswerList = Object.keys(mysrk).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...mysrk[key], params: data });
	return val;
}, {});
// 设为精品 get
const quality = Object.keys(mysrk).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...mysrk[key], params: data });
	return val;
}, {});
// 取消精品 get
const deleteQuality = Object.keys(mysrk).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...mysrk[key], params: data });
	return val;
}, {});
// 取消精品 get
const shield = Object.keys(mysrk).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...mysrk[key], params: data });
	return val;
}, {});
// 取消精品 get
const deleteShield = Object.keys(mysrk).reduce((val, key) => {
	val[key] = (data = {}) => Axios({ ...mysrk[key], params: data });
	return val;
}, {});

const res = {
	loginData,
	homeData,
	interviewData,
	projectlistData,
	answerlist,
	questionlist,
	getData,
	addInterviewList,
	personData,
	answeraddd,
	ansDetail,
	recordlist,
	personDetailInfo,
	allList,
	collectionadd,
	collectiondelete,
	reply,
	deletelist,
	details,
	mysrkList,
	answerdelete,
	selectContextByAnswerId,
	update,
	myanswer,
	traininglistData,
	gettrainingData,
	projectTeapost,
	projectTeaget,
	answerwait,
	aswerall,
	replyRightAnswer,
	selectAnswerList,
	quality,
	deleteQuality,
	shield,
	deleteShield,
	waitWork,
	delStations,
};

export default res;

// const dirdata = require.context('./config', true, /\.js$/);

// const res1 = dirdata.keys().reduce((val, item) => {
// 	const apis = dirdata(item);

// 	const newSpaces = item.match(/\/(\w+)\.js$/)[1];

// 	val[newSpaces] = Object.keys(apis).reduce((val, key) => {
// 		val[key] = (data = {}) => {
// 			if (data?.params != {} && data?.params?.methods == 'get') {
// 				return Axios({ ...apis[key], params: data.params.obj });
// 			} else {
// 				return Axios({ ...apis[key], data })
// 			}
// 		};

// 		return val;
// 	}, {});

// 	return val;
// }, {});

// console.log(res1);
