import { createRouter, createWebHistory } from 'vue-router';
import NProgress from 'nprogress';
import routes from './routes.js';
import 'nprogress/nprogress.css';
// const identity = window.localStorage.getItem('identity');
const router = createRouter({
	history: createWebHistory(process.env.BASE_URL),
	routes,
});
const userRouter = ['login'];
router.beforeEach((to, from, next) => {
	const myRoutes = [
		'post',
		'project',
		'person',
		'training',
		'interview',
		'record',
		'myrecord',
		'ranking',
		'addrecord',
		'answer',
		'answerlist',
		'detail',
		'myanswer',
		'postdetail',
		'projectdetail',
		'recordetail',
		'planList',
		'viewPlan',
		'defence',
		'planListManage',
		'needHandle',
		'addPostSkill',
		'interviewManage',
		'teacherdetail',
		'teacheraddProjec',
		'addProject',
		'chart',
	];
	NProgress.start();

	if (userRouter.includes(to.name)) {
		next();
	}
	if (myRoutes.findIndex(name => name == to.name) != -1) {
		next();
	} else {
		next('/login');
	}
	document.title = to.meta.title;
});

router.afterEach(() => {
	NProgress.done();
});
export default router;
