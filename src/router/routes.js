const routes = [
	{
		path: '/home',
		name: 'home',
		meta: { title: '岗位' },
		redirect: '/home/post',
		component: () => import(/* webpackChunkName: "home" */ '../views/home.vue'),
		children: [
			{
				path: '/home/post',
				name: 'post',
				meta: { title: '岗位管理' },
				component: () =>
					import(/* webpackChunkName: "post" */ '../views/home/post/post.vue'),
			},
			{
				path: '/home/addPostSkill',
				name: 'addPostSkill',
				meta: { title: '添加岗位' },
				component: () =>
					import(
						/* webpackChunkName: "addPostSkill" */ '../views/home/post/addPostSkill/index.vue'
					),
			},
			{
				path: '/home/postdetail/:id',
				name: 'postdetail',
				meta: { title: '岗位/岗位能力画像' },
				component: () =>
					import(
						/* webpackChunkName: "postdetail" */ '../views/home/post/postDetail.vue'
					),
			},
			{
				path: '/home/project',
				name: 'project',
				meta: { title: '项目' },
				component: () =>
					import(
						/* webpackChunkName: "project" */ '../views/home/project/project.vue'
					),
			},
			{
				path: '/home/projectdetail/:id',
				name: 'projectdetail',
				meta: { title: '项目库' },
				component: () =>
					import(
						/* webpackChunkName: "projectdetail" */ '../views/home/project/projectDetail.vue'
					),
			},
			// 添加项目
			{
				path: '/home/addProject/:versionId/:proId/:see',
				name: 'addProject',
				meta: { title: '八维教育' },
				component: () =>
					import(
						/* webpackChunkName: "addProject" */ '../views/home/project/addProject.vue'
					),
			},
			{
				path: '/home/person',
				name: 'person',
				meta: { title: '个人中心' },
				component: () =>
					import(
						/* webpackChunkName: "person" */ '../views/home/person/person.vue'
					),
			},
			{
				path: '/home/needHandle',
				name: 'needHandle',
				meta: { title: '我的待办' },
				component: () =>
					import(
						/* webpackChunkName: "needHandle" */ '../views/home/needHandle/index.vue'
					),
			},
			{
				path: '/home/chart',
				name: 'chart',
				meta: { title: '我的图表' },
				component: () =>
					import(
						/* webpackChunkName: "chart" */ '../views/home/needHandle/vuechart.vue'
					),
			},
			{
				path: '/home/training',
				name: 'training',
				meta: { title: '实训' },
				component: () =>
					import(
						/* webpackChunkName: "training" */ '../views/home/training/training.vue'
					),
				children: [
					{
						path: '/home/training/planList',
						name: 'planList',
						meta: { title: '实训/计划' },
						component: () =>
							import(
								/* webpackChunkName: "planList" */ '../views/home/training/planList/index.vue'
							),
					},
					{
						path: '/home/training/viewPlan',
						name: 'viewPlan',
						meta: { title: '实训/进度' },
						component: () =>
							import(
								/* webpackChunkName: "viewPlan" */ '../views/home/training/viewPlan/index.vue'
							),
					},
					{
						path: '/home/training/defence',
						name: 'defence',
						meta: { title: '实训/答辩' },
						component: () =>
							import(
								/* webpackChunkName: "defence" */ '../views/home/training/defence/index.vue'
							),
					},
					{
						path: '/home/training/planListManage',
						name: 'planListManage',
						meta: { title: '实训/计划(管理)' },
						component: () =>
							import(
								/* webpackChunkName: "planListManage" */ '../views/home/training/planListManage/index.vue'
							),
					},
				],
			},
			{
				path: '/home/interview',
				name: 'interview',
				meta: { title: '面试' },
				component: () =>
					import(
						/* webpackChunkName: "interview" */ '../views/home/interview/interview.vue'
					),
				children: [
					{
						path: '/home/interview/record',
						name: 'record',
						meta: { title: '面试记录' },
						component: () =>
							import(
								/* webpackChunkName: "record" */ '../views/home/interview/record/record.vue'
							),
					},
					{
						path: '/home/interview/myrecord',
						name: 'myrecord',
						meta: { title: '我的面试记录' },
						component: () =>
							import(
								/* webpackChunkName: "myrecord" */ '../views/home/interview/myrecord/myrecord.vue'
							),
					},
					{
						path: '/home/interview/ranking',
						name: 'ranking',
						meta: { title: '面试排行榜' },
						component: () =>
							import(
								/* webpackChunkName: "ranking" */ '../views/home/interview/ranking/ranking.vue'
							),
					},
					{
						path: '/home/interview/interviewmanage',
						name: 'interviewManage',
						meta: { title: '面试记录管理' },
						component: () =>
							import(
								/* webpackChunkName: "interviewManage" */ '../views/home/interview/interviewManage/interviewManage.vue'
							),
					},
				],
			},
			{
				path: '/home/addrecord',
				name: 'addrecord',
				meta: { title: '添加面试记录' },
				component: () =>
					import(
						/* webpackChunkName: "addrecord" */ '../views/home/interview/addrecord/index.vue'
					),
			},
			{
				path: '/home/recordetail/:interviewId',
				name: 'recordetail',
				component: () =>
					import(
						/* webpackChunkName: "recordetail" */ '../views/home/interview/recordetail'
					),
			},
			{
				path: '/home/answer',
				name: 'answer',
				meta: { title: '问答' },
				component: () =>
					import(
						/* webpackChunkName: "answer" */ '../views/home/answer/answer.vue'
					),
				children: [
					{
						path: '/home/answer/answerlist',
						name: 'answerlist',
						meta: { title: '问答列表' },
						component: () =>
							import(
								/* webpackChunkName: "answerlist" */ '../views/home/answer/answerlist/answerlist.vue'
							),
					},
					{
						path: '/home/answer/detail/:answerId',
						name: 'detail',
						meta: { title: '问答详情' },
						component: () =>
							import(
								/* webpackChunkName: "detail" */ '../views/home/answer/detail/detail.vue'
							),
					},
					{
						path: '/home/answer/myanswer',
						name: 'myanswer',
						meta: { title: '我的问答' },
						component: () =>
							import(
								/* webpackChunkName: "myanswer" */ '../views/home/answer/myanswer/myanswer.vue'
							),
					},
				],
			},
		],
	},
	{
		path: '/login',
		name: 'login',
		meta: { title: '登录' },
		component: () =>
			import(/* webpackChunkName: "login" */ '../views/login.vue'),
	},
	{
		path: '/',
		redirect: '/login',
	},
];

export default routes;
